package com.example.citty.scorekeepervolley;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    //Variables for the scores
    private int RallyPointB = 0;
    private int RallyPointA = 0;
    //Variables for the sets
    private int setA = 0;
    private int setB = 0;
    //The Serving String Variable
    private String service = "Serving";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    /*
        This method add a Rally point to team A
     */
    public void addRallyPointA(View view) {
        RallyPointA++;
        displayScoreForTeamA(RallyPointA,setA);
        /*
            Give the service to the Team A
         */
        displayService(service, (TextView) findViewById(R.id.team_a_service));
        displayService("",(TextView) findViewById(R.id.team_b_service));
    }
    /*
        This method add a Set to team A and reset the points
     */
    public void addSetA(View view) {
        setA ++ ;
        resetRallyPoints();
    }
    /*
        This method add a Rally Point to team B
     */
    public void addRallyPointB(View view) {
        RallyPointB++;
        displayScoreForTeamB(RallyPointB,setB);
        /*
            Give the service to the Team B
         */
        displayService(service, (TextView) findViewById(R.id.team_b_service));
        displayService("",(TextView) findViewById(R.id.team_a_service));
    }
    /*
        This method add a Set to team B and reset the points
     */
    public void addSetB(View view) {
        setB ++;
        resetRallyPoints();
    }
    /*
        This method display the team A scores
    */
    public void displayScoreForTeamA(int score, int set) {
        TextView scoreView = findViewById(R.id.team_a_score);
        scoreView.setText(String.valueOf(score));
        TextView setView = findViewById(R.id.team_a_set);
        setView.setText(String.valueOf(set));
    }
    /*
        This method display the team B scores
    */
    public void displayScoreForTeamB(int score, int set) {
        TextView scoreView = findViewById(R.id.team_b_score);
        scoreView.setText(String.valueOf(score));
        TextView setView = findViewById(R.id.team_b_set);
        setView.setText(String.valueOf(set));
    }
    public void displayService(String string, TextView view) {
        view.setText(string);
    }
    /*
    This method reset the scores for a new game
     */
    public void resetScore(View view) {
        setB = 0;
        setA = 0;
        resetRallyPoints();
        displayService("",(TextView) findViewById(R.id.team_a_service));
        displayService("",(TextView) findViewById(R.id.team_b_service));
    }
    /*
    This method reset the Rally points of the teams
     */
    public void resetRallyPoints() {
        RallyPointB = 0;
        RallyPointA = 0;
        displayScoreForTeamB(RallyPointB,setB);
        displayScoreForTeamA(RallyPointA,setA);
    }

}
